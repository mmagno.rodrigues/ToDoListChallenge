//
//  MainViewController.swift
//  ToDoList
//
//  Created by Administrator on 20/06/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    override func viewDidLoad() {
        super.viewDidLoad()
        taskTableView.delegate = self
        taskTableView.dataSource = self
        
        self.tasksDict.getDictionary()
    }

    //MARK: Properties
    var tasksDict = TaskDictionary()
    var task_a: Task?
    @IBOutlet weak var taskTableView: UITableView!

    //MARK: SUper methods implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        return tasksDict.getNumSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sections = tasksDict.getSections()
        let sectionStr = sections[section]
        return tasksDict.getNumTasks(sect: sectionStr)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TaskViewCell else{
            fatalError("problem queueing cell view")
        }
        
        let taskToShow = self.tasksDict.taskFromIndexPath(indexPath: indexPath)
        cell.taskDescrip.text = taskToShow.descript
        
        var sections: String = ""
        for sect in taskToShow.sections!{
            sections += sect
        }
        
        
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToDetail"{
            let detailViewController = segue.destination as! DetailViewController
            let indexPath = taskTableView.indexPathForSelectedRow!
            let cellTask = self.tasksDict.taskFromIndexPath(indexPath: indexPath)
            
            detailViewController.task = cellTask
        }
    }
    
    
}
