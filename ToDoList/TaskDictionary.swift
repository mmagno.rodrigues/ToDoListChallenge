//
//  TaskDictionary.swift
//  ToDoList
//
//  Created by Administrator on 19/06/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import Foundation

class TaskDictionary{
    init(){
        self.dict = [:]
    }
    
    //MARK: Public Interface
    public func taskFromIndexPath(indexPath: IndexPath)->Task{
        getDictionary()
        let sect = getSections()[indexPath.section]
        let taskIndex = indexPath.row
        
        return (self.dict[sect]!)[taskIndex]
    }
    
    public func removeTask(task: Task){
        getDictionary()
        let sections = task.sections
        for sect in sections!{
            let tasksSect = self.dict[sect]
            var i = 0
            for taskIn in tasksSect!{
                if taskIn.descript == task.descript, (self.dict[sect]?.count)! > i{
                    (self.dict[sect])?.remove(at: i)
                    i += 1
                }
            }
        }
        setDictionary()
    }
    
    public func getSections() -> [String]{
        getDictionary()
        return [String] (self.dict.keys)
    }
    
    public func getNumSections() -> Int{
        getDictionary()
        return self.dict.count
    }
    
    public func getNumTasks() -> Int{
        getDictionary()
        var taskCount = 0
        for(_, taskArray) in self.dict{
            taskCount += taskArray.count
        }
        return taskCount
    }
    
    public func getNumTasks(sect: String) -> Int{
        getDictionary()
        return self.dict[sect]!.count
    }
    
    public func getSectionTasks(sect: String) -> [Task]{
        getDictionary()
        return self.dict[sect]!
    }
    
    public func addTask(newTask: Task){
        getDictionary()
        if newTask.sections! == []{
            if self.dict["default"] == nil{
                self.dict["default"] = []
            }
                self.dict["default"]!.append(newTask)
        }
        else{
            for sect in newTask.sections!{
                if self.dict[sect] == nil{
                    self.dict[sect] = []
                }
                self.dict[sect]?.append(newTask)
            }
        }
        setDictionary()
    }
    //MARK: Proprerties
    private var dict : Dictionary<String, Array<Task>>
    private var defaultsDictionaryKey = "taskDictionary"
    
    //MARK: PrivateInterface
    func setDictionary(){
        let encodedDict = NSKeyedArchiver.archivedData(withRootObject: self.dict)
        UserDefaults.standard.set(encodedDict, forKey: defaultsDictionaryKey)
    }
    
    func getDictionary(){
        guard let dicionario = UserDefaults.standard.value(forKey: defaultsDictionaryKey) else{
            self.dict = [:]
            return
        }
        let decodedDict = NSKeyedUnarchiver.unarchiveObject(with: dicionario as! Data)
        self.dict = decodedDict as! Dictionary<String, Array<Task>>
    }
}
