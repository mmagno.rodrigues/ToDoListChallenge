//
//  Task.swift
//  ToDoList
//
//  Created by Administrator on 19/06/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import Foundation

class Task: NSObject, NSCoding{
    init(descript : String?, sections: [String]?){
        self.descript = descript
        self.sections = sections
    }
    
    //MARK: NSCoding
    required convenience init?(coder decoder: NSCoder) {
        let descript = decoder.decodeObject(forKey: "descript") as! String
        let sections = decoder.decodeObject(forKey: "sections") as! [String]
        
        self.init(descript:descript, sections:sections)
    }
    func encode(with coder: NSCoder) {
        coder.encode(self.descript, forKey: "descript")
        coder.encode(self.sections, forKey: "sections")
    }
    
    //MARK: Properties
    var descript : String?
    var sections : [String]?
}
