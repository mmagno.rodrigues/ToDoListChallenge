//
//  DetailViewController.swift
//  ToDoList
//
//  Created by Administrator on 21/06/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITextViewDelegate, UITextInputTraits {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.descript.text = task?.descript
        var sections = ""
        for sect in (task?.sections!)!{
            sections += sect + " "
        }
        self.sections.text = sections
    }
    
    //MARK: Properties
    var task: Task?
    @IBOutlet weak var descript: UILabel!
    @IBOutlet weak var sections: UILabel!
    var deleted = false

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromDetail"{
            let mainViewController = segue.destination as! MainViewController
            if deleted {
                mainViewController.tasksDict.removeTask(task: task!)
            }
            
        }
    }
    
    //MARK: Actions
    @IBAction func deleteButton(_ sender: Any) {
        deleted = true
    }
    
}
