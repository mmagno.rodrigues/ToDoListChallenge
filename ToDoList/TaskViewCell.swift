//
//  TaskViewCell.swift
//  ToDoList
//
//  Created by Administrator on 21/06/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class TaskViewCell: UITableViewCell {
    
    @IBOutlet weak var taskDescrip: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
