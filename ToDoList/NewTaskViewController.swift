//
//  NewTaskViewController.swift
//  ToDoList
//
//  Created by Administrator on 20/06/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class NewTaskViewController: UIViewController, UITextViewDelegate, UITextInputTraits {
    override func viewDidLoad() {
        super.viewDidLoad()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NewTaskViewController.dismissKeyboard))
        
        tap.cancelsTouchesInView = true
        
        view.addGestureRecognizer(tap)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: Properties
    @IBOutlet weak var priorityCheckBox: UIButton!
    var priorityCheck = false
    @IBOutlet weak var sectionsTextView: UITextView!
    @IBOutlet weak var taskTextView: UITextView!
    var task : Task?
    //******************************************
    
    private func getTaskSections(sections: String) -> [String]{
        return [sections]
    }
    
    
    //MARK: Actions
    @IBAction func okAddButton(_ sender: Any) {
        var sections: [String] = getTaskSections(sections: sectionsTextView.text)
        if priorityCheck{
            sections.append("Importante")
        }
        task = Task(descript: taskTextView.text, sections: sections)        
        performSegue(withIdentifier: "addedSegue", sender: self)
    }
    
    @IBAction func cancelAddButton(_ sender: Any) {
    }
    
    @IBAction func priorityCBAct(_ sender: Any) {
        if priorityCheck == false{
            self.priorityCheckBox.setImage(#imageLiteral(resourceName: "checked"), for: UIControlState.normal)
            priorityCheck = true
        }else{
            self.priorityCheckBox.setImage(#imageLiteral(resourceName: "unchecked"), for: UIControlState.normal)
            priorityCheck = false
        }
    }
    
    func dismissKeyboard(){
        view.endEditing(true)
    }
    //*******************************************
    
    //MARK: UITextViewDelegate
    func textViewDidEndEditing(_ textView: UITextView) {
        sectionsTextView.resignFirstResponder()
        taskTextView.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sectionsTextView.resignFirstResponder()
        taskTextView.resignFirstResponder()
        return true
    }
    //******************************************


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addedSegue"{
            let mainViewController = segue.destination as! MainViewController
            mainViewController.tasksDict.addTask(newTask: self.task!)
            
        }
    }
   
  

}
